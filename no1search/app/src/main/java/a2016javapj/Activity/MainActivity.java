package a2016javapj.Activity;

import android.Manifest;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import a2016javapj.API.ImageSearchTask;
import a2016javapj.API.SearchTask;
import a2016javapj.API.URLCreator;
import a2016javapj.Process.CameraProcess;
import a2016javapj.Process.CropProcess;
import a2016javapj.Process.GalleryProcess;
import a2016javapj.Others.ItemList;
import a2016javapj.Process.R;
import a2016javapj.Utils.FileUtils;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {

    private static final int REQUEST_CAPTURE_IMAGE = 100;
    private static final int REQUEST_GALLERY_IMAGE = 200;
    private static final int REQUEST_CROP_IMAGE = 300;
    private static final String SEARCH_TASK_FLAG = "searchTask finished";

    private ImageView imageView;
    private Spinner spinner;
    private Uri imageUri;
    private Uri tmpUri;
    private Bitmap cropBitmap;
//    private Uri croppedUri;
//    private List<Uri> UriList = new ArrayList<>();
    private final String LOGTAG = getClass().getSimpleName();
    private ProgressDialog progressDialog;
    private String imageSearchResponse;
    private ImageSearchTask imageLoader;
    private SearchTask searchLoader;
    private AlertDialog.Builder alertDialog;
    private int siteId;

    CameraProcess cameraProcess = new CameraProcess();
    GalleryProcess galleryProcess = new GalleryProcess();
    CropProcess cropProcess = new CropProcess();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
        progressDialog = new ProgressDialog(this);
        alertDialog = new AlertDialog.Builder(this);

        // Android 6, API 23以上でパーミッシンの確認
        if (Build.VERSION.SDK_INT >= 23) {
            checkPermission();
        }
    }

    /*
     * アプリ起動時のビュー設定
     */
    private void setView() {
        // 初期画像
        imageView = (ImageView) findViewById(R.id.camera_image);
        imageView.setImageResource(R.drawable.logo);

        // カメラ起動ボタン
        ImageButton cameraBtn = (ImageButton) findViewById(R.id.camera_btn);
        cameraBtn.setOnClickListener(cameraListener);

        // ギャラリーボタン
        ImageButton galleryBtn = (ImageButton) findViewById(R.id.gallery_btn);
        galleryBtn.setOnClickListener(galleryListener);

        // 再認識ボタン
        ImageButton retryBtn = (ImageButton) findViewById(R.id.retry_btn);
        retryBtn.setOnClickListener(retryListener);

        // 検索ボタン
        ImageButton searchBtn = (ImageButton) findViewById(R.id.search_btn);
        searchBtn.setOnClickListener(searchListener);

        // サイト選択
        ImageButton siteBtn = (ImageButton) findViewById(R.id.site_btn);
        spinner = (Spinner) findViewById(R.id.site_spinner);
        siteBtn.setOnClickListener(siteListener);

    }

    /*
     * カメラ起動ボタンクリックリスナー
     */
    private View.OnClickListener cameraListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // カメラ起動
            Log.d(LOGTAG, "camera process start");
            Intent cameraIntent = cameraProcess.startCamera();
            tmpUri = cameraIntent.getExtras().getParcelable(MediaStore.EXTRA_OUTPUT);
            startActivityForResult(cameraIntent, REQUEST_CAPTURE_IMAGE);
        }
    };

    /*
     * ギャラリーボタンクリックリスナー
     */
    private View.OnClickListener galleryListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // ギャラリー呼び出し
            Log.d(LOGTAG, "gallery process start");
            Intent galleryIntent = galleryProcess.startGallery();
            startActivityForResult(galleryIntent, REQUEST_GALLERY_IMAGE);
        }
    };

    /*
     * 再認識ボタンクリックリスナー
     */
    private View.OnClickListener retryListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(imageUri == null || cropBitmap == null) {
                Log.w(LOGTAG, "image was not choose");
                alertDialog.setMessage(getResources().getString(R.string.alert_no_image));
                alertDialog.setPositiveButton(getResources().getString(R.string.positive_button), null);
                alertDialog.show();
                return;
            }
            // トリミングやり直し
            cropImage(imageUri);
        }
    };


    /*
     * 検索ボタンクリックリスナー
     */
    private View.OnClickListener searchListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(imageUri == null || cropBitmap == null) {
                Log.w(LOGTAG, "image was not choose");
                alertDialog.setMessage(getResources().getString(R.string.alert_no_image));
                alertDialog.setPositiveButton(getResources().getString(R.string.positive_button), null);
                alertDialog.show();
                return;
            }

            getLoaderManager().restartLoader(1, null, MainActivity.this);
        }
    };

    /*
     * サイト選択ボタンクリックリスナー
     */
    private View.OnClickListener siteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            spinner.performClick();
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                        siteId = position;
                        showToast((String) adapter.getSelectedItem());
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) {}
                }
            );
        }
    };

    /*
     * 画像トリミング処理
     */
    private void cropImage(Uri uri) {
        Log.d(LOGTAG, "crop process started");
        Intent cropIntent = cropProcess.startCrop(uri);
        startActivityForResult(cropIntent, REQUEST_CROP_IMAGE);
    }

    /*
     * アクション結果
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        // リクエスト判定
        switch (requestCode) {
            // カメラ
            case REQUEST_CAPTURE_IMAGE:
                if(resultCode != RESULT_OK) return;
                imageUri = tmpUri;
                Log.d(LOGTAG, "image uri from camera : " + imageUri);
                cropImage(imageUri);
                break;
            // ギャラリー
            case REQUEST_GALLERY_IMAGE:
                if(resultCode != RESULT_OK) return;
                imageUri = resultData.getData();
                Log.d(LOGTAG, "image uri from gallery : " + imageUri);
                cropImage(imageUri);
                break;
            case REQUEST_CROP_IMAGE:
                if(resultCode != RESULT_OK) return;
                cropBitmap = resultData.getExtras().getParcelable("data");
//                仕様変更⇒トリミングした一時ファイルは残さないため、コメントアウト
//                croppedUri = cropProcess.saveCroppedBitmap(cropBitmap);
//                アプリ停止時のファイル削除のため、Uriを保存
//                UriList.add(croppedUri);
                imageView = (ImageView) findViewById(R.id.camera_image);
                imageView.setImageBitmap(cropBitmap);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        一時ファイルの仕様変更により削除処理もコメントアウト
//        FileUtils.deleteImages(UriList);
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        switch (id) {
            // 画像認識API呼び出し
            case 1:
                Log.d(LOGTAG, "image search task call");
                progressDialog.setMessage(getResources().getString(R.string.message_progressing));
                progressDialog.show();
                imageLoader = new ImageSearchTask(this.getApplicationContext(), cropBitmap);
                imageLoader.forceLoad();
                return imageLoader;
            // 検索API呼び出し
            case 2:
                Log.d(LOGTAG, "search task call");
                // URLを作成、Loaderを実行
                URLCreator creator = new URLCreator();
                URL url= null;
                TypedArray array = getResources().obtainTypedArray(R.array.sites);
                // デフォルトサイトは楽天
                int resourceId = array.getResourceId(siteId, R.string.rakuten);
                Log.i(LOGTAG, "selected site : " + getResources().getString(resourceId));
                switch(resourceId) {
                    case R.string.rakuten:
                        url = creator.createRakutenURL(imageSearchResponse);
                        break;
                    default:
                        Log.w(LOGTAG, "not listed site. change to default : " + getResources().getString(resourceId) + " → 楽天");
                        // caseに設定していないリソースはデフォルトに変換
                        url = creator.createRakutenURL(imageSearchResponse);
                        break;
                }
                array.recycle();
                searchLoader = new SearchTask(this.getApplicationContext(), url);
                searchLoader.forceLoad();
                return searchLoader;
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String response) {
        if(response == SEARCH_TASK_FLAG){
            Log.d(LOGTAG, "search task finished");
            ArrayList<ItemList> itemList = searchLoader.getItemList();
            if(itemList.size() == 0) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setMessage(getResources().getString(R.string.alert_no_result));
                alertDialog.setPositiveButton(getResources().getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        progressDialog.dismiss();
                    }
                });
                alertDialog.show();
            } else {
                // 画面遷移
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                intent.putExtra("itemList", itemList);
                startActivity(intent);
                progressDialog.dismiss();
            }
        } else {
            Log.d(LOGTAG, "image search task finished");
            // 色の取得のため、再度認識結果をパース
            String[] parseRes = FileUtils.parseDelimiter(response);
            Log.d(LOGTAG, "parsed image search response : " + Arrays.toString(parseRes));
            if(parseRes.length >= 2) {
                if(parseRes[1].length() == 0) {
                    Log.w(LOGTAG, "label is null, stop processing");
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setMessage(getResources().getString(R.string.alert_no_result));
                    alertDialog.setPositiveButton(getResources().getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            progressDialog.dismiss();
                        }
                    });
                    alertDialog.show();
                    return;
                }
                StringBuilder sb = new StringBuilder();
                for(String str : parseRes) {
                    sb.append(str);
                    sb.append(" ");
                }
                imageSearchResponse = sb.toString();
                Log.i(LOGTAG, "image search response : " + imageSearchResponse);
                getLoaderManager().restartLoader(2, null, MainActivity.this);
            } else {
                Log.w(LOGTAG, "image search result is not enough : " + parseRes.length);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setMessage(getResources().getString(R.string.network_error));
                alertDialog.setPositiveButton(getResources().getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        progressDialog.dismiss();
                    }
                });
                alertDialog.show();
                return;
            }
        }
    }

    private void showToast(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER,0,370);
        toast.show();
    }
    @Override
    public void onLoaderReset(Loader<String> loader) {
        Log.w(LOGTAG, "onLoaderReset call");
    }

    // permissionの確認
    public void checkPermission() {
        // 拒否している場合
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED){
            requestLocationPermission();
        }
    }

    // 許可を求める
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1000);
        } else {
            Toast toast =
                    Toast.makeText(this, "アプリ実行に許可が必要です", Toast.LENGTH_SHORT);
            toast.show();

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,},
                    1000);
        }
    }
}
