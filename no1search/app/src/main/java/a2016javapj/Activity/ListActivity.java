package a2016javapj.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.net.Uri;

import java.util.ArrayList;

import a2016javapj.Others.ItemList;
import a2016javapj.Others.ItemListAdapter;
import a2016javapj.Process.R;

/**
 * Created by yong on 2017/06/06.
 */

public class ListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private final String LOGTAG = getClass().getSimpleName();
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searched_list);
        listView = (ListView)findViewById(R.id.list_view);
        setList();
    }

    private void setList() {
        ArrayList<ItemList> itemList = getIntent().getParcelableArrayListExtra("itemList");
        ItemListAdapter adapter = new ItemListAdapter(this, R.layout.list_inside, itemList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        ItemList itemList = (ItemList)listView.getItemAtPosition(position);
        String uriStr = itemList.getUriStr();
        Uri uri = Uri.parse(uriStr);
        Intent intent = new Intent(Intent.ACTION_VIEW,uri);
        startActivity(intent);
    }
}
