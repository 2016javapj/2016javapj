package a2016javapj.Initialize;

import android.content.Context;

/**
 * Created by yong on 2018/03/15.
 */

public class MyContext {
    private static MyContext instance = null;
    private Context aplContext;

    static void onCreateApl(Context aplContext) {
        instance = new MyContext(aplContext);
    }

    private MyContext(Context aplContext) {
        this.aplContext = aplContext;
    }

    public static MyContext getInstance() {
        if(instance == null) {
            throw new RuntimeException("MyContext should be initialized!");
        }
        return instance;
    }

    public Context getAplContext() {
        if(aplContext == null) {
            throw new RuntimeException("Application Context should be initialized!");
        }
        return aplContext;
    }
}
