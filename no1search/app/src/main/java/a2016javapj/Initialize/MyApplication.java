package a2016javapj.Initialize;

import android.app.Application;
import android.util.Log;

/**
 * Created by yong on 2018/03/15.
 */

public class MyApplication extends Application {

    private final String LOGTAG = getClass().getSimpleName();

    @Override
    public void onCreate() {
        Log.d(LOGTAG,"MyApplication onCreate");
        super.onCreate();

        MyContext.onCreateApl(getApplicationContext());
    }
}
