package a2016javapj.Others;


import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class ItemList implements Parcelable {
    private String itemName;
    private String uriStr;
    private Bitmap thumbnail;

    /*
     * コンストラクタ
     */
    public ItemList(String itemName, String uriStr, Bitmap thumbnail) {
        this.itemName = itemName;
        this.uriStr = uriStr;
        this.thumbnail = thumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(itemName);
        out.writeString(uriStr);
        out.writeParcelable(thumbnail, flags);
    }

    public static final Parcelable.Creator<ItemList> CREATOR
            = new Parcelable.Creator<ItemList>() {
        public ItemList createFromParcel(Parcel in) {
            return new ItemList(in);
        }

        public ItemList[] newArray(int size) {
            return new ItemList[size];
        }
    };

    private ItemList(Parcel in) {
        itemName = in.readString();
        uriStr = in.readString();
        thumbnail = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public String getItemName() {
        return itemName;
    }

    public String getUriStr() {
        return uriStr;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

}
