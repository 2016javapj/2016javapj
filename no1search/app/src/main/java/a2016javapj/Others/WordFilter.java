package a2016javapj.Others;

import java.util.Arrays;
import java.util.List;

/**
 * Created by jun on 2017-11-15.
 */

public class WordFilter {
    // 除外単語配列
    private final List<String> sExceptWords = Arrays.asList("product", "bag", "black", "red", "blue", "green", "white", "orange",
            "coat", "denim", "footwear", "shoe");

    // 除外判定処理
    public boolean checkExcept(final String sCheckWord) {
        boolean bExcept = false;    // 除外フラグ

        for (String sWord: sExceptWords) {
            if (sWord.equals(sCheckWord)) {
                bExcept = true;
                break;
            }
        }
        return bExcept;
    }
}
