package a2016javapj.Others;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import a2016javapj.Others.ItemList;
import a2016javapj.Process.R;

public class ItemListAdapter extends ArrayAdapter<ItemList> {
    private int resource;
    private List<ItemList> itemList;
    private LayoutInflater inflater;

    /*
     * コンストラクタ
     */
    public ItemListAdapter(Context context, int resource, List<ItemList> itemList) {
        super(context, resource, itemList);

        this.resource = resource;
        this.itemList = itemList;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if(convertView != null) {
            view = convertView;
        } else {
            view = this.inflater.inflate(this.resource, null);
        }

        // リストビューに表示する要素取得
        ItemList item = this.itemList.get(position);

        // サムネイル画像設定
        ImageView thumbnailView = (ImageView)view.findViewById(R.id.thumbnail);
        thumbnailView.setImageBitmap(item.getThumbnail());

        // タイトル設定
        TextView itemNameView = (TextView)view.findViewById(R.id.itemName);
        itemNameView.setText(item.getItemName());

        return view;
    }
}
