package a2016javapj.Others;

/**
 * Created by yong on 2017/09/10.
 */

public enum Colors {
    WHITE(255, 255, 255),
    OLIVE(128, 128, 0),
    YELLOW(255, 255, 0),
    PINK(255, 0, 255),
    SILVER(192, 192, 192),
    AQUA(0, 255, 255),
    LIME(0, 255, 0),
    RED(255, 0, 0),
    GRAY(128, 128, 128),
    BLUE(0, 0, 255),
    GREEN(0, 128, 0),
    PURPLE(128, 0, 128),
    BLACK(0, 0, 0),
    NAVY(0, 0, 128),
    TEAL(0, 128, 128),
    MAROON(128, 0, 0);

    private final int r;
    private final int g;
    private final int b;

    private Colors(final int r, final int g, final int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public int getRed() {
        return r;
    }

    public int getGreen() {
        return g;
    }

    public int getBlue() {
        return b;
    }
}