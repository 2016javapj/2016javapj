package a2016javapj.API;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import a2016javapj.Activity.MainActivity;
import a2016javapj.Others.ItemList;
import a2016javapj.Process.R;

/**
 * Created by yong on 2017/06/03.
 */

public class SearchTask extends AsyncTaskLoader<String> {

    private final String LOGTAG = getClass().getSimpleName();
    private static final String SEARCH_TASK_FLAG = "searchTask finished";

    private URL url;
    private ArrayList<ItemList> itemList = new ArrayList<>();
    private MainActivity activity = new MainActivity();

    public SearchTask(Context context, URL url){
        super(context);
        this.url = url;
    }

    @Override
    public String loadInBackground() {
        Log.i(LOGTAG, "search API loadInBackground start");
        long start = System.currentTimeMillis();

        URLConnection conn = null;
        InputStream is = null;
        BufferedReader br = null;

        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.connect();

            is = conn.getInputStream();

            StringBuilder sb = new StringBuilder();
            String resString = null;
            JSONObject json = null;

            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            while((resString = br.readLine()) != null) {
                sb.append(resString);
            }
            json = new JSONObject(sb.toString());

            Log.i(LOGTAG, "response : " + json.toString());

            // TODO 検索サイトの選択による分岐を追加する
            parseRakuten(json);

        } catch(Exception e) {
            Log.e(LOGTAG, "doInBackground error : " + e);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setMessage(activity.getResources().getString(R.string.network_error));
            alertDialog.setPositiveButton(activity.getResources().getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            alertDialog.show();
        } finally {
            if(br != null) {
                try{
                    br.close();
                } catch(Exception e) {
                    Log.e(LOGTAG, "BufferedReader close error : " + e);
                }
            }
            if(is != null) {
                try {
                    is.close();
                } catch(IOException e) {
                    Log.e(LOGTAG, "InputStream close error : " + e);
                }
            }
        }
        System.setProperty("http.keepAlive", "false");

        Log.i(LOGTAG, "search API end");
        Log.i(LOGTAG, "search TAT : " + String.valueOf(System.currentTimeMillis() - start));

        return SEARCH_TASK_FLAG;
    }

    public void parseRakuten(JSONObject response) throws JSONException, MalformedURLException, IOException {
        URLConnection conn = null;

        // 取得したJSONデータをパース
        int hits = response.getInt("hits");
        JSONArray items = response.getJSONArray("Items");
        for(int i = 0; i < hits; i++) {
            JSONObject item = items.getJSONObject(i).getJSONObject("Item");
            String itemName = item.getString("itemName");
            String itemURL = item.getString("itemUrl");
            String imageURL = item.getJSONArray("smallImageUrls").getJSONObject(0).getString("imageUrl");

            Log.d(LOGTAG, "item name : " + itemName);
            Log.d(LOGTAG, "item url : " + itemURL);
            Log.d(LOGTAG, "image url : " + imageURL);

            // URLからイメージのbitmap取得
            url = new URL(imageURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.connect();

            InputStream imageStream = conn.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
            ItemList itemView = new ItemList(itemName, itemURL, bitmap);
            itemList.add(itemView);
        }
    }

    public ArrayList<ItemList> getItemList() {
        return itemList;
    }
}
