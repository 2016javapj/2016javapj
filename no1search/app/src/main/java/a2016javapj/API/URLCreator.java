package a2016javapj.API;

import android.net.Uri;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by yong on 2017/06/03.
 */

public class URLCreator {

    private final String LOGTAG = getClass().getSimpleName();

    private final String SCHEMA = "https";
    private final String AUTHORITY = "app.rakuten.co.jp";
    private final String PATH = "/services/api/IchibaItem/Search/20140222";
    private final String APP_ID = "1061190936384893141";

    public URL createRakutenURL(String result) {
        // 画像認識結果を検索キーワードとして設定
        String keyword = result;

        Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.scheme(SCHEMA);
        uriBuilder.authority(AUTHORITY);
        uriBuilder.path(PATH);
        uriBuilder.appendQueryParameter("applicationId", APP_ID);
        uriBuilder.appendQueryParameter("keyword", keyword);

        String uri = uriBuilder.toString();
        Log.i(LOGTAG, "Rakuten request URL : " + uri);
        URL url = null;
        try {
            url = new URL(uri);
        } catch (MalformedURLException e) {
            Log.e(LOGTAG, "Rakuten URL create error");
        }

        return url;
    }
}
