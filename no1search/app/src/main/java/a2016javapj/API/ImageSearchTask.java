package a2016javapj.API;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.VisionRequest;
import com.google.api.services.vision.v1.VisionRequestInitializer;
import com.google.api.services.vision.v1.model.AnnotateImageRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;
import com.google.api.services.vision.v1.model.ColorInfo;
import com.google.api.services.vision.v1.model.DominantColorsAnnotation;
import com.google.api.services.vision.v1.model.EntityAnnotation;
import com.google.api.services.vision.v1.model.Feature;
import com.google.api.services.vision.v1.model.Image;
import com.google.api.services.vision.v1.model.ImageProperties;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import a2016javapj.Others.Colors;
import a2016javapj.Utils.FileUtils;
import a2016javapj.Others.WordFilter;
import a2016javapj.Utils.PackageManagerUtils;

/**
 * Created by jun on 2017-06-17.
 */

public class ImageSearchTask extends AsyncTaskLoader<String> {
    private static final String CLOUD_VISION_API_KEY = "AIzaSyDjN-fLvZyZXfbY2ttAkqbRfzD-ygJwSWI";
    private static final String ANDROID_CERT_HEADER = "X-Android-Cert";
    private static final String ANDROID_PACKAGE_HEADER = "X-Android-Package";
    private final String LOGTAG = getClass().getSimpleName();
    private Bitmap bitmap = null;
    private double dLimit = 0.8;
    private static final String DELIMETER = "|";

    public ImageSearchTask(Context context, Bitmap bitmap) {
        super(context);
        this.bitmap = bitmap;
    }
    
    @Override
    public String loadInBackground() {
        Log.d(LOGTAG, "image search API loadInBackground start");
        String sResult = "";
        long start = System.currentTimeMillis();

        try {
            HttpTransport httpTransport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = GsonFactory.getDefaultInstance();

            VisionRequestInitializer requestInitializer =
                    new VisionRequestInitializer(CLOUD_VISION_API_KEY) {
                        // APIキーを使ってヘッダーに識別フィールドを格納
                        @Override
                        protected void initializeVisionRequest(VisionRequest<?> visionRequest)
                                throws IOException {
                            super.initializeVisionRequest(visionRequest);
                            String packageName = getContext().getPackageName();
                            visionRequest.getRequestHeaders().set(ANDROID_PACKAGE_HEADER, packageName);

                            String sig = PackageManagerUtils.getSignature(getContext().getPackageManager(), packageName);
                            visionRequest.getRequestHeaders().set(ANDROID_CERT_HEADER, sig);
                        }
                    };

            Vision.Builder builder = new Vision.Builder(httpTransport, jsonFactory, null);
            builder.setVisionRequestInitializer(requestInitializer);

            Vision vision = builder.build();

            BatchAnnotateImagesRequest batchAnnotateImagesRequest = new BatchAnnotateImagesRequest();
            batchAnnotateImagesRequest.setRequests(new ArrayList<AnnotateImageRequest>() {{
                AnnotateImageRequest annotateImageRequest = new AnnotateImageRequest();

                // 画像ファイルの追加
                Image base64EncodedImage = new Image();
                // bitmapをJPEGに変換
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
                byte[] imageBytes = byteArrayOutputStream.toByteArray();

                // Base64に エンコード
                base64EncodedImage.encodeContent(imageBytes);
                annotateImageRequest.setImage(base64EncodedImage);

                // 必要なデータを選択
                annotateImageRequest.setFeatures(new ArrayList<Feature>() {{
                    Feature labelDetection = new Feature();
                    labelDetection.setType("LABEL_DETECTION");
                    labelDetection.setMaxResults(10);

                    Feature imageDetection = new Feature();
                    imageDetection.setType("IMAGE_PROPERTIES");
                    imageDetection.setMaxResults(1);

                    Feature logoDetection = new Feature();
                    logoDetection.setType("LOGO_DETECTION");
                    logoDetection.setMaxResults(1);

                    add(imageDetection);
                    add(labelDetection);
                    add(logoDetection);

                }});
                // リクエストを追加
                add(annotateImageRequest);
            }});

            Vision.Images.Annotate annotateRequest =
                    vision.images().annotate(batchAnnotateImagesRequest);
            // gzipファイル、大きな画像を含まれてる場合、APIへのリクエストは失敗します。
            annotateRequest.setDisableGZipContent(true);
            Log.d(LOGTAG, "created Cloud Vision request object, sending request");

            //TODO 初回のexecuteが時間かかる?⇒アプリ起動時に一度走らせる？⇒ネットの環境によるっぽい
            BatchAnnotateImagesResponse response = annotateRequest.execute();

            sResult = convertResponseToString(response);
            Log.i(LOGTAG, "image search API end. reuslt : " + sResult);
            Log.i(LOGTAG, "image search TAT : " + String.valueOf(System.currentTimeMillis() - start));
        } catch (GoogleJsonResponseException e) {
            Log.e(LOGTAG, "failed to make API request because " + e.getContent());
        } catch (IOException e) {
            Log.e(LOGTAG, "failed to make API request because of other IOException " + e.getMessage());
        }
        return sResult;
    }

    private String convertResponseToString(BatchAnnotateImagesResponse response) {
        WordFilter wordFilter = new WordFilter();
        StringBuilder sb = new StringBuilder();

        List<EntityAnnotation> labels = response.getResponses().get(0).getLabelAnnotations();
        List<EntityAnnotation> logos = response.getResponses().get(0).getLogoAnnotations();
        ImageProperties colors = response.getResponses().get(0).getImagePropertiesAnnotation();
//        response.getResponses().get(0);

        if (logos != null) {
            for (EntityAnnotation logo : logos) {
                String sScore = String.format(Locale.US, "%.3f", logo.getScore());
                String sLogo = String.format(Locale.US, "%s", logo.getDescription());
                double dScore = Double.parseDouble(sScore.trim());
                if (dScore > dLimit) {
                    Log.d(LOGTAG, "logo : " + sLogo);
                    sb.append(sLogo);
                    sb.append(DELIMETER);
                    break;
                } else {
                    Log.w(LOGTAG, "logo's all scores are lower than 90%");
                    sb.append(DELIMETER);
                    break;
                }
            }
        } else {
            Log.d(LOGTAG, "logo is null");
            sb.append(DELIMETER);
        }
        if (labels != null) {
            for (EntityAnnotation label : labels) {
//                message += String.format(Locale.US, "%.3f: %s", label.getScore(), label.getDescription());
//                message += "\n";
                String sScore = String.format(Locale.US, "%.3f", label.getScore());
                String sLabel = String.format(Locale.US, "%s", label.getDescription());
                double dScore = Double.parseDouble(sScore.trim());

                // 除外単語判定
                if (!wordFilter.checkExcept(sLabel)) {
                    if (dScore > dLimit) {
                        Log.d(LOGTAG, "label : " + sLabel);
                        sb.append(sLabel);
                        sb.append(DELIMETER);
                        break;
                    } else {
                        Log.w(LOGTAG, "label's all scores are lower than 80%");
                        sb.append(DELIMETER);
                        break;
                    }
                }
            }
            Log.d(LOGTAG, "##########################################################");
            for (EntityAnnotation label : labels) {
//                message += String.format(Locale.US, "%.3f: %s", label.getScore(), label.getDescription());
//                message += "\n";
                String sScore = String.format(Locale.US, "%.3f", label.getScore());
                String sLabel = String.format(Locale.US, "%s", label.getDescription());
                double dScore = Double.parseDouble(sScore.trim());

                Log.d(LOGTAG, "label : " + sLabel + "   score : " + sScore);
            }
            Log.d(LOGTAG, "##########################################################");
        } else {
            Log.d(LOGTAG, "label is null");
            sb.append(DELIMETER);
        }
        if (colors != null) {
            DominantColorsAnnotation color = colors.getDominantColors();
            for (ColorInfo rgb : color.getColors()) {
                String sRed = FileUtils.parseRGB(String.valueOf(rgb.getColor().getRed()));
                String sGreen = FileUtils.parseRGB(String.valueOf(rgb.getColor().getGreen()));
                String sBlue = FileUtils.parseRGB(String.valueOf(rgb.getColor().getBlue()));
                Log.d(LOGTAG, "red : " + sRed + ", green : " + sGreen + ", blue : " + sBlue);
                String colorName = findColorName(Integer.parseInt(sRed), Integer.parseInt(sGreen), Integer.parseInt(sBlue));
                sb.append(colorName);
                Log.d(LOGTAG, "##########################################################");
                Log.d(LOGTAG, "##########################################################");
                Log.d(LOGTAG, "ColorName : " + colorName);
                Log.d(LOGTAG, "##########################################################");
                Log.d(LOGTAG, "##########################################################");
                break;
            }
        } else {
            Log.w(LOGTAG, "color is null");
        }
        String message = sb.toString();
        return message;
    }

    public String findColorName(int r, int g, int b) {
        // RGB情報取得
        Colors[] colors = Colors.values();
        HashMap<String, int[]> colorMap = new HashMap<String, int[]>();
        for(Colors color : colors) {
            String name = color.name();
            int red = color.getRed();
            int green = color.getGreen();
            int blue = color.getBlue();
            int[] rgb = {red, green, blue};

            colorMap.put(name, rgb);
        }

        // rが一番近い色に絞る
        HashMap<String, int[]> nearR = new HashMap<String, int[]>();
        for(int i = 1; i < 256; i++) {
            if(r == 0 || r == 128 || r == 192 || r == 255) {
                for(String name : colorMap.keySet()) {
                    int[] rgb = colorMap.get(name);
                    if(rgb[0] == r) {
                        nearR.put(name, rgb);
                    }
                }
                break;
            } else {
                r = (i % 2 == 0)? r+i : r-i;
            }
        }
        Log.d(LOGTAG, "find nearest red : " + r + ", selected colors : " + nearR.keySet().toString());

        // rで絞った中からgの近い値を絞る
        HashMap<String, int[]> nearRg = new HashMap<String, int[]>();
        for(int i = 1; i < 256; i++) {
            if (g == 0 || g == 128 || g == 192 || g == 255) {
                for (String name : nearR.keySet()) {
                    int[] rgb = nearR.get(name);
                    if (rgb[1] == g) {
                        nearRg.put(name, rgb);
                    }
                }
                break;
            } else {
                g = (i % 2 == 0)? g+i : g-i;
            }

        }
        Log.d(LOGTAG, "find nearest green : " + g + ", selected colors : " + nearRg.keySet().toString());

        // rgで絞った中からbの近い値を絞る
        HashMap<String, int[]> nearRgb = new HashMap<String, int[]>();
        for(int i = 1; i < 256; i++) {
            if (b == 0 || b == 128 || b == 192 || b == 255) {
                for (String name : nearRg.keySet()) {
                    int[] rgb = nearRg.get(name);
                    if (rgb[2] == b) {
                        nearRgb.put(name, rgb);
                    }
                }
                break;
            } else {
                b = (i % 2 == 0)? b+i : b-i;
            }
        }
        Log.d(LOGTAG, "find nearest blue : " + b + ", finally selected color : " + nearRgb.keySet().toString());

        String resultColor = "";
        if(nearRgb.size() == 1) {
            String colorName = nearRgb.keySet().toString();
            resultColor = colorName.substring(1, colorName.length()-1);
            Log.i(LOGTAG, "found color name : " + resultColor);
        } else {
            Log.i(LOGTAG, "found color result is null");
        }
        return resultColor;
    }
}