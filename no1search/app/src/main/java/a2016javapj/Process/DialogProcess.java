package a2016javapj.Process;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

/**
 * Created by yong on 2017/09/09.
 */

public class DialogProcess extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle("タイトル")
                .setMessage("メッセージ")
                .setPositiveButton("OK", null)
                .create();
    }

    @Override
    public void onPause() {
        super.onPause();
        dismiss();
    }
}
