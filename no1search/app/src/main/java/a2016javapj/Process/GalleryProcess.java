package a2016javapj.Process;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * ギャラリー関連処理クラス
 */

public class GalleryProcess extends AppCompatActivity {

    private final String LOGTAG = getClass().getSimpleName();

    /*
     * ギャラリー呼び出し
     */
    public Intent startGallery() {
        // インテント生成
        Intent galleryIntent;

        //SDKのバージョンによってインテントの設定が異なる
        if (Build.VERSION.SDK_INT < 19) {
            Log.d(LOGTAG, "SDK version : " + Build.VERSION.SDK_INT);
            galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            galleryIntent.setType("image/*");
        } else {
            Log.d(LOGTAG, "SDK version : " + Build.VERSION.SDK_INT);
            galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            galleryIntent.setType("image/*");
        }
        return galleryIntent;
    }
}
