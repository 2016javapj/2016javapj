package a2016javapj.Process;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * 画像トリミングクラス
 */

public class CropProcess extends AppCompatActivity {

    private final String LOGTAG = getClass().getSimpleName();

    /*
     * トリミング画面移動
     */
    public Intent startCrop(Uri uri) {
        if (uri == null) {
            Log.w(LOGTAG, "uri is null");
            return null;
        }
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        cropIntent.setDataAndType(uri, "image/*");
        cropIntent.putExtra("crop", true);
        cropIntent.putExtra("outputX", 200);
        cropIntent.putExtra("outputY", 200);
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("scale", true);
        cropIntent.putExtra("return-data", true);
        return cropIntent;
    }

    /*
     * トリミングしたBitmapデータの保存処理
     * 仕様変更のため、一応使わない予定
     */
//    public Uri saveCroppedBitmap(Bitmap image) {
//        Log.d(LOGTAG, "save cropped bitmap to file");
//        if(image != null) {
//            File imageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), FileUtils.createCropImageName());
//            FileOutputStream fos = null;
//
//            try {
//                fos = new FileOutputStream(imageFile);
//                image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
//            } catch (IOException e) {
//                Log.e(LOGTAG, e.getMessage());
//            } finally {
//                if (fos != null) {
//                    try {
//                        fos.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//            return Uri.fromFile(imageFile);
//        } else {
//            return null;
//        }
//    }
}
