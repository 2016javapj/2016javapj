package a2016javapj.Process;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import a2016javapj.Initialize.MyContext;
import a2016javapj.Utils.FileUtils;

/**
 * カメラ関連処理クラス
 */

public class CameraProcess extends AppCompatActivity {

    private File file;
    private final String LOGTAG = getClass().getSimpleName();

    /*
     * カメラ起動、画像保存
     */
    public Intent startCamera() {
        String parentDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        String fileDir = parentDir + "/DCIM/Camera/";
        File file = new File(fileDir, FileUtils.createImageName());

        // インテント生成
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(isExternalStorageWritable()) {
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this, "no1search.provider", file));
        } else {
            Toast toast = Toast.makeText(this, "写真を保存できません", Toast.LENGTH_SHORT);
            toast.show();
        }

        return cameraIntent;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }
}
