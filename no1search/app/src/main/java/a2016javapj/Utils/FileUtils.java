package a2016javapj.Utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class FileUtils {
    private final static String LOGTAG = "FileUtils";
    private static final String DELIMETER = "\\|";
    private static final String COLOR_DELIMETER = ".";

    /*
     * 画像ファイル名作成
     */
    public static String createImageName() {
        String dateStr = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.JAPAN).format(new Date());
        String fileName = "no1search_" + dateStr + ".jpg";
        Log.i(LOGTAG, "create file name : " + fileName);
        return fileName;
    }

    /*
     * 編集ファイル名作成
     */
    public static String createCropImageName() {
        String dateStr = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.JAPAN).format(new Date());
        String tmpFileName = "no1search_cropped_" + dateStr + ".jpg";
        Log.i(LOGTAG, "create cropped file name : " + tmpFileName);
        return tmpFileName;
    }

    /*
     * 画像削除処理
     * 一時ファイルの仕様変更によりコメントアウト
     */
//    public static void deleteImages(List<Uri> UriList) {
//        Log.i(LOGTAG, "delete uri count : " + UriList.size());
//        Log.i(LOGTAG, "delete uri list : " + UriList.toString());
//        for(int i = 0; i < UriList.size(); i++){
//            String path = UriList.get(i).getPath();
//            File file = new File(path);
//            if(file.exists()) {
//                file.delete();
//            } else {
//                Log.w(LOGTAG, "file not exist : " + file.getName());
//            }
//        }
//    }

    /*
     * 画像認識結果のパース処理
     */
    public static String[] parseDelimiter(String response) {
        String[] parseRes = response.split(DELIMETER);
        return parseRes;
    }

    /*
     * RGBの.を消す(80.0 → 80)
     */
    public static String parseRGB(String color) {
        int indexNum = color.indexOf(COLOR_DELIMETER);
        String parseColor = color.substring(0, indexNum);
        return parseColor;
    }
    /*
     * 画像認識のRGBからbitmap取得
     */
    public static Bitmap rgbToBitmap(int red, int green, int blue) {
        Bitmap bitmap = Bitmap.createBitmap(72, 72, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawRGB(red, green, blue);

        return bitmap;
    }
}

